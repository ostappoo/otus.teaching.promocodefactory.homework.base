﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T> : IRepository<T> where T : BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }


        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public async Task CreateEntityAsync(T entity)
        {
            var result = Data.ToList();
            result.Add(entity);
            Data = result;
            await Task.CompletedTask;
        }

        public async Task DeleteEntityAsync(Guid id)
        {
            var dataList = Data.ToList();
            dataList.Remove(dataList.First(x=>x.Id == id));
            Data = dataList;
            await Task.CompletedTask;
        }

        public async Task PatchEntityAsync(T entity)
        {
            var dataList = Data.ToList();

            var updating = dataList.Where(x => x.Id == entity.Id).FirstOrDefault();
            updating = entity;


            Data = dataList;
            await Task.CompletedTask;
        }
    }
}